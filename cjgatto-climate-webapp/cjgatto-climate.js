Climate_Data = new Mongo.Collection('temp_data');
Yearly_Averages = new Mongo.Collection('yearly_avg_data');

var map;
var year = 1950;

if (Meteor.isClient) {
  // Display all climate data
  Template.body.helpers({
    temp_data: function () {
      return Climate_Data.find({});
    }
  });

  // Initialize world map
  Template.activity_map.rendered = function () {
     map = new Datamap({
      element: document.getElementById('map'),
      scope: 'usa',
      geographyConfig: {
        popupTemplate: function(geography, data) {
          return '<div class="hoverinfo">' + geography.properties.name + ': ' + data.temp + ' '
        },
        highlightBorderWidth: 3
      },
      fills: {
        defaultFill: "#ABDDA4",
        no_data: "#555555",
        freezing: "#0000FF",
        cold: "#00FFFF",
        mild: "#00FF00",
        warm: "#FFFF00",
        hot: "#FF0000",
        error: "#990099"
      },
      data: {}
    });
    Template.activity_map.map = map;
  };

  // Initialize year slider
  Template.slider.rendered = function () {
    initSlider();
    draw_temperature_data();
  }

  initSlider = function() {
    d3.select('#year_slider').call(d3.slider().axis(true).min(1950).max(2000).step(5).on("slide", function(evt, value) {
      d3.select('#year_slidertext').text(value);
      d3.select('#average_text').text(Yearly_Averages.findOne({_id: value}, {avgTemp:1}).avgTemp);
      year = value;
      draw_temperature_data();
    }));
  }

  var colors = d3.scale.category10();

  function draw_temperature_data() {
    map.updateChoropleth({
      AZ: { fillKey: getTempColor("AZ"), temp: getTemp("AZ") },
      CO: { fillKey: getTempColor("CO"), temp: getTemp("CO") },
      DE: { fillKey: getTempColor("DE"), temp: getTemp("DE") },
      FL: { fillKey: getTempColor("FL"), temp: getTemp("FL") },
      GA: { fillKey: getTempColor("GA"), temp: getTemp("GA") },
      HI: { fillKey: getTempColor("HI"), temp: getTemp("HI") },
      ID: { fillKey: getTempColor("ID"), temp: getTemp("ID") },
      IL: { fillKey: getTempColor("IL"), temp: getTemp("IL") },
      IN: { fillKey: getTempColor("IN"), temp: getTemp("IN") },
      IA: { fillKey: getTempColor("IA"), temp: getTemp("IA") },
      KS: { fillKey: getTempColor("KS"), temp: getTemp("KS") },
      KY: { fillKey: getTempColor("KY"), temp: getTemp("KY") },
      LA: { fillKey: getTempColor("LA"), temp: getTemp("LA") },
      MD: { fillKey: getTempColor("MD"), temp: getTemp("MD") },
      ME: { fillKey: getTempColor("ME"), temp: getTemp("ME") },
      MA: { fillKey: getTempColor("MA"), temp: getTemp("MA") },
      MN: { fillKey: getTempColor("MN"), temp: getTemp("MN") },
      MI: { fillKey: getTempColor("MI"), temp: getTemp("MI") },
      MS: { fillKey: getTempColor("MS"), temp: getTemp("MS") },
      MO: { fillKey: getTempColor("MO"), temp: getTemp("MO") },
      MT: { fillKey: getTempColor("MT"), temp: getTemp("MT") },
      NC: { fillKey: getTempColor("NC"), temp: getTemp("NC") },
      NE: { fillKey: getTempColor("NE"), temp: getTemp("NE") },
      NV: { fillKey: getTempColor("NV"), temp: getTemp("NV") },
      NH: { fillKey: getTempColor("NH"), temp: getTemp("NH") },
      NJ: { fillKey: getTempColor("NJ"), temp: getTemp("NJ") },
      NY: { fillKey: getTempColor("NY"), temp: getTemp("NY") },
      ND: { fillKey: getTempColor("ND"), temp: getTemp("ND") },
      NM: { fillKey: getTempColor("NM"), temp: getTemp("NM") },
      OH: { fillKey: getTempColor("OH"), temp: getTemp("OH") },
      OK: { fillKey: getTempColor("OK"), temp: getTemp("OK") },
      OR: { fillKey: getTempColor("OR"), temp: getTemp("OR") },
      PA: { fillKey: getTempColor("PA"), temp: getTemp("PA") },
      RI: { fillKey: getTempColor("RI"), temp: getTemp("RI") },
      SC: { fillKey: getTempColor("SC"), temp: getTemp("SC") },
      SD: { fillKey: getTempColor("SD"), temp: getTemp("SD") },
      TN: { fillKey: getTempColor("TN"), temp: getTemp("TN") },
      TX: { fillKey: getTempColor("TX"), temp: getTemp("TX") },
      UT: { fillKey: getTempColor("UT"), temp: getTemp("UT") },
      WI: { fillKey: getTempColor("WI"), temp: getTemp("WI") },
      VA: { fillKey: getTempColor("VA"), temp: getTemp("VA") },
      VT: { fillKey: getTempColor("VT"), temp: getTemp("VT") },
      WA: { fillKey: getTempColor("WA"), temp: getTemp("WA") },
      WV: { fillKey: getTempColor("WV"), temp: getTemp("WV") },
      WY: { fillKey: getTempColor("WY"), temp: getTemp("WY") },
      CA: { fillKey: getTempColor("CA"), temp: getTemp("CA") },
      CT: { fillKey: getTempColor("CT"), temp: getTemp("CT") },
      AK: { fillKey: getTempColor("AK"), temp: getTemp("AK") },
      AR: { fillKey: getTempColor("AR"), temp: getTemp("AR") },
      AL: { fillKey: getTempColor("AL"), temp: getTemp("AL") }
    });
  }

  function getTempColor(state) {
    var avg_doc = Yearly_Averages.findOne({_id: year}, {avgTemp:1});
    var temp_doc = Climate_Data.findOne({year: year, state: state}, {avg:1});
    var diff;

    if(typeof avg_doc === 'undefined' || typeof temp_doc === 'undefined') {
      return 'no_data';
    }
    else {
      diff = parseFloat(temp_doc.avg) - parseFloat(avg_doc.avgTemp);

      if(diff < -5) {
        return 'freezing';
      }
      else if(diff < -2.5) {
        return 'cold';
      }
      else if(diff < 2.5) {
        return 'mild';
      }
      else if(diff < 5) {
        return 'warm';
      }
      else if(diff > 5) {
        return 'hot';
      }
      else {
        return 'error';
      }
    }
  }

  function getTemp(state) {
    var temp_doc = Climate_Data.findOne({year: year, state: state}, {avg:1});

    if(typeof temp_doc === 'undefined') {
      return "No Data"
    }
    else {
      return temp_doc.avg + " F";
    }
  }
}
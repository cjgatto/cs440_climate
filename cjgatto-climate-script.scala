val sqlContext = new org.apache.spark.sql.SQLContext(sc)
import sqlContext.createSchemaRDD
import scala.collection.mutable.ArrayBuffer

/*
	Station Data:
	Station data for all weather stations in West Virginia are collected
	using Spark RDDs. The resulting data is stored in wvStations.

	Station Schema:
	"USAF","WBAN","STATION NAME","CTRY","FIPS","STATE","CALL","LAT","LON","ELEV(.1M)"
*/
val stationsFile = sc.textFile("s3n://cs440-climate/ish-history.csv")

def format_station_data (str : String) : String = {
	return str.replace("\"", "").trim
}

case class Station (
	usaf: String,
	country: String,
	state: String
)

val stations = stationsFile.filter(line => !line.contains("USAF")).map(_.split(",")).map(s => Station(
	format_station_data(s(0)), // usaf
	format_station_data(s(3)), // country
	format_station_data(s(5))  // state
))

val usStations = stations.filter(s => s.country == "US" && s.state != "" && s.usaf != "999999") // 5440
usStations.registerTempTable("usStations")

val usIDsBuffer = ArrayBuffer[String]()
usStations.toArray.foreach(s => usIDsBuffer += s.usaf)
val usIDs = usIDsBuffer.toSet

/*
	Weather Data:

	Weather Schema: (Note: STN corresponds to USAF from the stations' data)
	(0) STN---, (1) WBAN, (2) YEARMODA, (3) TEMP, (4) ?, (5) DEWP, (6) ?, (7) SLP, (8) ?, (9) STP, (10) ?,
	(11) VISIB, (12) ?, (13) WDSP , (14) ?, (15) MXSPD, (16) GUST, (17) MAX , (18) MIN, (19) PRCP, (20) SNDP,
	(21) FRSHTT
*/
val weatherFiles_1950_95 = sc.textFile("s3n://cs440-climate/gsod/19[56789][05]")
val weatherFiles_2000 = sc.textFile("s3n://cs440-climate/gsod/2000")
val weatherFiles = sc.union(Seq(weatherFiles_1950_95, weatherFiles_2000))

case class WeatherData (
	stn: String, 
	year: String, 
	temperature: String  
)

val weatherDatas = weatherFiles.filter(line => !line.contains("STN---")).map(_.replaceAll("\\s+", " ").split(" ")).map(wd => WeatherData(
	wd(0), // stn
	wd(2).substring(0, 4), // year
	wd(3)  // temperature
))

val usWeatherDatas = weatherDatas.filter(wd => usIDs.contains(wd.stn))
usWeatherDatas.registerTempTable("usWeatherDatas")

val avgWeatherData = sqlContext.sql("SELECT year, state, avg(temperature) FROM usWeatherDatas JOIN usStations ON usaf = stn GROUP BY year, state")

val jsonWeatherData = avgWeatherData.toJSON.saveAsTextFile("s3n://cjgatto-climate-data/usTempAvg")